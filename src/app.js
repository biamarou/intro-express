import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { setupDB } from './db';

export const app = express();
import projects from './projects';

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/projects', projects);
app.disable('x-powered-by');

export async function bootstrap() {
  const db = await setupDB();

  const handler = (req, res, next) => {
    res.json({ message: 'hello world' });
  };

  app.get('/', handler);
  return db;
}
