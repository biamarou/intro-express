import express from 'express';
import { setupDB } from './db';
const projectRouter = express.Router();

const findProject = async (req, res, next) => {
    const db = await setupDB();
    const { slug } = req.params;
    const project = await db.collection('projects').findOne({ slug });

    if (!project) {
        res.status(400).json({ error: `project ${slug} does not exist` });
    } else {
        req.body.project = project;
        next();
    }
};

const findBoard = async (req, res, next) => {
    const { project } = req.body;
    const { name } = req.params;
    const board_index = project.boards.findIndex(board => board.name === name);

    if (board_index === -1) {
        res.status(400).json({ error: `there is no ${name} board in project ${project.slug}` });
    } else {
        req.body.board_index = board_index;
        next();
    }
};

const findTask = async (req, res, next) => {
    const { project, board_index } = req.body;
    const { id, name } = req.params;
    const task_index = project.boards[board_index].tasks.findIndex(task => task.id === id);

    if (task_index === -1) {
        res.status(400).json({ error: `there is no ${id} task in board ${name}` });
    } else {
        req.body.task_index = task_index;
        next();
    }
};

projectRouter.post('/', async (req, res) => {
    const db = await setupDB();
    const { name } = req.body;
    const slug = name.toLowerCase();
    const project = {
        slug,
        boards: [],
    };

    const existingProject = await db.collection('projects').findOne({ slug });

    if (existingProject) {
        res.status(400).json({ error: `project ${slug} already exists` });
    } else {
        await db.collection('projects').insertOne(project);
        res.json({ project });
    }
});

projectRouter.get('/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    res.json({ project });
});

projectRouter.delete('/:slug', findProject, async (req, res) => {
    const db = await setupDB();
    const { project } = req.body;
    const removed_project = await db.collection('projects')
                                    .deleteOne({ slug: project.slug });
    res.json({ removed_project });
});

projectRouter.post('/:slug/boards', findProject, async (req, res) => {
    const db = await setupDB();
    const { name, project } = req.body;
    const board = { name, tasks: [] };

    project.boards.push(board);
    await db
     .collection('projects')
     .findOneAndReplace({ slug: project.slug }, project);

    res.json({ board });
});

projectRouter.post('/:slug/boards/:name/tasks', findProject, findBoard, async (req, res) => {
    const db = await setupDB();
    const { id, definition, project, board_index } = req.body;
    const task = { id, definition };

    project.boards[board_index].tasks.push(task);
    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ task });
});

projectRouter.delete('/:slug/boards/:name', findProject, findBoard, async (req, res) => {
    const db = await setupDB();
    const {project, board_index} = req.body
    const boards = project.boards;
    project.boards.splice(board_index, 1);

    await db
     .collection('projects')
     .findOneAndReplace({ slug: project.slug }, project);

    res.json({ boards });

});

projectRouter.delete('/:slug/boards/:name/tasks/:id', findProject, findBoard, findTask, async (req, res) => {
    const db = await setupDB();
    const { project, board_index, task_index } = req.body;
    const tasks = project.boards[board_index].tasks;

    project.boards[board_index].tasks.splice(task_index, 1);
    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ tasks });
});

export default projectRouter;