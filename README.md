# Intro ao Express

Repositório Dockerizado, para rodar

```bash
# constrói a imagem
docker-compose build

# roda em modo 'development'
docker-compose up
```

Para rodar os testes

```
docker-compose run server yarn test [--watchAll]
```

> `--watchAll` observa seu código por alterações e, quando elas acontecerem, roda a bateria de testes novamente
