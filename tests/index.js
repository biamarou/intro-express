import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';
    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  // delete project with slug
  test('DELETE /projects/:slug remove a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });
    const { body } = await request(app).delete('/projects/awesome');
    expect(body.removed_project.deletedCount).toBe(1);
  });

  // delete board with given name from project with given slug
  test('DELETE /projects/:slug/boards/:name remove board from a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';
    await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    const { body } = await request(app).delete('/projects/awesome/boards/todo');
    expect(body.boards).toBeDefined();
    expect(body.boards.findIndex(board => board.name === name)).toBe(-1);
  });

  // insert task into board with given name from project with given slug
  test('POST /projects/:slug/boards/:name/tasks adds a new task into a board from a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';
    await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    const id = 'id123';
    const definition = 'blah';
    const { body } = await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`)
      .send({ id, definition});

    expect(body.task).toBeDefined();
    expect(body.task.id).toBe(id);
    expect(body.task.definition).toBe(definition);
  });

  // delete task from board with given name from project with given slug
  test('DELETE /projects/:slug/boards/:name/tasks/:id remove task from a board from a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';
    await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    const id = 'id123';
    const definition = 'blah';
    await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`)
      .send({ id, definition});

    const { body } = await request(app).delete('/projects/awesome/boards/todo/tasks/id123');
    expect(body.tasks).toBeDefined();
    expect(body.tasks.findIndex(task => task.id === id)).toBe(-1);
  });
});
